<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClientRepository")
 */
class Client
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $Status;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $User;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private $Updated;

    public function getId()
    {
        return $this->id;
    }

    public function getStatus(): ?int
    {
        return $this->Status;
    }

    public function setStatus(int $Status): self
    {
        $this->Status = $Status;

        return $this;
    }

    public function getUser(): ?int
    {
        return $this->User;
    }

    public function setUser(?int $User): self
    {
        $this->User = $User;

        return $this;
    }

    public function getUpdated(): ?\DateTimeImmutable
    {
        return $this->Updated;
    }

    public function setUpdated(?\DateTimeImmutable $Updated): self
    {
        $this->Updated = $Updated;

        return $this;
    }
}
