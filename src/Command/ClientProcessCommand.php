<?php

namespace App\Command;

use App\Entity\Client;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;

class ClientProcessCommand extends ContainerAwareCommand
{
    protected static $defaultName = 'ClientProcess';
    protected const OPER1 = 1;
    protected const OPER2 = 2;

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Fill the table with default values or view them all')
            ->addOption('status', null, InputOption::VALUE_REQUIRED, 'status value(0,1,2,3,4,5)');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine')->getManager();


        if (!$input->getOption('status') && $input->getFirstArgument('arg1')) {
            switch ($input->getArgument('arg1')) {
                case 'fill':
                    $this->fillTable();
                    break;
                case 'view':
                    $this->viewTable($output);
                    break;
                case 'operator1':
                    $this->processClient($this->getClient(static::OPER1), self::OPER1, $input, $output);
                    break;
                case 'operator2':
                    $this->processClient($this->getClient(static::OPER2), self::OPER2, $input, $output);
                    break;
                default:
                    $output->writeln('Error in arguments! exiting...');
                    break;
            }
            // Завершаем скрипт.
            exit;
        }

        if ($input->getOption('status')) {
            $status = $input->getOption('status');

            $clients = $em->getRepository(Client::class)->findAll();

        }

        $io->success('You have a new command! Now make it your own! Pass --help to see your options.');
    }

    /**
     * @return EntityManager
     */
    protected function getEm()
    {
        return $this->getContainer()->get('doctrine')->getManager();
    }

    protected function fillTable()
    {
        $em = $this->getEm();
        $em->beginTransaction();
        for ($i = 0; $i < 10; $i++) {
            $client = new Client();
            $client->setStatus(0);
            $client->setUser(null);
            $em->persist($client);
        }
        $em->commit();
        $em->flush();
    }

    protected function viewTable(OutputInterface $output)
    {
        $em = $this->getEm();
        $clients = $em->getRepository(Client::class)->findAll();
        foreach ($clients as $client) {
            $updated = $client->getUpdated() ? $client->getUpdated()->format('l dS F Y') : 'none';

            $output->writeln('ClientId: ' . $client->getId() . ', Status: ' . $client->getStatus() .
                ', Updated: ' . $updated);
        }

    }

    /**
     * @return null|Client
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function getClient($operator)
    {
        $client =
            $this->getEm()->getRepository(Client::class)
                ->findOneBy([], ['Updated' => 'asc']);
        $client->setUpdated(new \DateTimeImmutable('now'));
        $client->setUser($operator);
        var_dump('oper:' . $operator);
        $this->getEm()->persist($client);
        $this->getEm()->flush();
        return $client;
    }

    protected function processClient(Client $client, $operator, InputInterface $input, OutputInterface $output) {
        $helper = $this->getHelper('question');
        $answValues =[0 => 'no call', 1 => 'closed', 'busy', 'answer', 'ask to recall', 'denied'];
        $question = new ChoiceQuestion('select client status:',
            $answValues);
        $question->setErrorMessage('Status is invalid');
        $status = array_flip($answValues)[$helper->ask($input, $output, $question)];

        $client->setStatus($status);
        $this->getEm()->persist($client);
        $this->getEm()->flush($client);

        $output->writeln('Status set to: ' . $status);
        exit;
    }

}
